#!/bin/bash
cd /workspace/
/external/bin/thrift1 --templates /external/include/thrift/templates --gen mstch_cpp2 HelloWorld.thrift
cmake .
make -j $(nproc)
