#include <iostream>
#include <memory>
#include "gen-cpp2/PrintHelloWorld.h"
#include <thrift/lib/cpp/async/TAsyncSocket.h>
#include <thrift/lib/cpp2/async/HeaderClientChannel.h>
#include <folly/init/Init.h>
#include <folly/executors/IOThreadPoolExecutor.h>

using namespace std;
using namespace apache::thrift;
using namespace helloworld;
using namespace folly;


int main(int argc,char ** argv){
    folly::init(&argc, &argv);
    auto io_executor_ = std::make_shared<folly::IOThreadPoolExecutor>(2);
    auto sendone = [&] (folly::Unit u) {
        auto client = folly::make_unique<PrintHelloWorldAsyncClient>(
                HeaderClientChannel::newChannel(
                        async::TAsyncSocket::newSocket(
                                io_executor_->getEventBase(), {"127.0.0.1", 9090})));
        auto fut = client->future_sayHello("FutureTom");
        return fut;
    };

    folly::via(io_executor_.get()).thenValue(sendone).thenValue([&](const string &res) {
        std::cout << res << std::endl;
    }).wait();

    return 0;
}

