## FBthrift server and client sample code

The sample uses a pre-built fbthrift docker image to build server and client talking via fbthrift.

1. The image uses fbthrift/folly version 2018.10.29.00, where there is no longer "Future::then()". Use "thenTry" or "thenValue" instead.
2. By default folly::via(executor) returns folly::Unit. Therefore lambda function in "via().thenValue(f)" must take folly::Unit as arguement.
3. Bitbucket Pipelines work great!

