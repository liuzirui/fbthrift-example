ln -s /external/src/mstch-1.0.2 /external/src/mstch
ln -s /external/src/folly-2018.10.29.00 /external/src/folly
ln -s /external/src/fizz-2018.10.29.00 /external/src/fizz
ln -s /external/src/wangle-2018.10.29.00 /external/src/wangle
ln -s /external/src/fbthrift-2018.10.29.00 /external/src/fbthrift

ls -l /external/src

apt update -yq
apt install -yq  g++ \
  git \
  cmake \
  bison \
  flex \
  automake \
  autoconf \
  autoconf-archive \
  pkg-config \
  libtool \
  libboost-all-dev \
  libevent-dev && apt clean
apt install -yq \
  libdouble-conversion-dev \
  libgoogle-glog-dev \
  libgflags-dev \
  liblz4-dev \
  liblzma-dev \
  libsnappy-dev \
  make \
  zlib1g-dev \
  binutils-dev \
  libjemalloc-dev \
  libssl-dev \
  libiberty-dev \
  libzstd-dev \
  libkrb5-dev \
  libsodium-dev && apt clean

cd /external/src
mkdir folly_build && cd folly_build && cmake -DCMAKE_INSTALL_PREFIX=/external -DBUILD_TESTS=off ../folly && make -j $(nproc) all install && cd /external/src && rm -fr folly_build && rm -fr folly/.git

cd /external/src
mkdir mstch_build && cd mstch_build && cmake -DCMAKE_INSTALL_PREFIX=/external -DWITH_UNIT_TESTS=off -DWITH_BENCHMARK=off ../mstch && make -j $(nproc) all install && cd /external/src && rm -fr mstch_build && rm -fr mstch/.git

cd /external/src
mkdir fizz_build && cd fizz_build && cmake -DCMAKE_INSTALL_PREFIX=/external -DBUILD_TESTS=off ../fizz/fizz && make -j $(nproc) all install && cd /external/src && rm -fr fizz_build && rm -fr fizz/.git

cd /external/src
mkdir wangle_build && cd wangle_build && cmake -DCMAKE_INSTALL_PREFIX=/external -DBUILD_TESTS=off ../wangle/wangle && make -j $(nproc) all install && cd /external/src && rm -fr wangle_build && rm -fr wangle/.git

cd /external/src
mkdir fbthrift_build && cd fbthrift_build && cmake -DCMAKE_INSTALL_PREFIX=/external -DBUILD_TESTS=off ../fbthrift && make -j $(nproc) all install && cd /external/src && rm -fr fbthrift_build && rm -fr fbthrift/.git

