#include <iostream>
#include <memory>
#include "gen-cpp2/PrintHelloWorld.h"
#include <thrift/lib/cpp2/server/ThriftServer.h>
#include <folly/init/Init.h>
using namespace std;
using namespace apache::thrift;
using namespace helloworld;

int i = 1;

class HelloWorldSvImpl : public PrintHelloWorldSvIf{

void printHelloWorld(){
    cout<<"Hello World"<<endl;
}
/* 此处为Server的同步接口
void sayHello(std::string& result, std::unique_ptr<std::string> name_in){
    result = name_in->c_str();
    result += ", Hello";
    cout<<"Called:" << (++i) <<endl;
}
*/
void async_tm_sayHello(std::unique_ptr<apache::thrift::HandlerCallback<std::unique_ptr<std::string>>> callback, std::unique_ptr<std::string> name) {
    std::string save = *name;
    save += ", Hello Async";
    callback->result(save);
    cout<<"Called:" << (i++) <<endl;
}

};

int main(int argc,char ** argv){
folly::init(&argc, &argv);
auto handler = make_shared<HelloWorldSvImpl>();
auto server = make_shared<ThriftServer>();
server->setInterface(handler);
server->setPort(9090);
server->setNumIOWorkerThreads(4);
printf("Starting the server...\n");
server->serve();
return 0;
}
