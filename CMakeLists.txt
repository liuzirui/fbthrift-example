cmake_minimum_required(VERSION 3.5)
project(simple)

set(CMAKE_CXX_STANDARD 14)

include_directories(gen-cpp2)
include_directories(/external/include)
link_directories(/external/lib)

add_executable(helloclient
        gen-cpp2/PrintHelloWorldAsyncClient.cpp
        helloclient.cpp)

target_link_libraries(helloclient thrift thriftcpp2 thriftprotocol transport thriftfrozen2 async concurrency protocol thrift-core wangle fizz sodium folly glog ssl crypto event pthread gflags iberty dl boost_context double-conversion lzma zstd z snappy lz4 boost_system krb5 gssapi_krb5)

add_executable(helloserver
        gen-cpp2/PrintHelloWorld.cpp
        helloserver.cpp)

target_link_libraries(helloserver thrift thriftcpp2 thriftprotocol transport thriftfrozen2 async concurrency protocol thrift-core wangle fizz sodium folly glog ssl crypto event pthread gflags iberty dl boost_context double-conversion lzma zstd z snappy lz4 boost_system krb5 gssapi_krb5)
